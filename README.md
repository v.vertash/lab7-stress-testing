# Lab7 -- Stress testing 

## Result tests report
 - CalculatePrice minute: 6279 (33.736%)
 - GetSpec: 6226 (33.452%)
 - CalculatePrice fixed: 6107 (32.812%)
 
## Screenshots

### Artillery
 ![Artillery report 1](./artillery_results_png/artillery_1.png)
 ![Artillery report 2](./artillery_results_png/artillery_2.png)
 ![Artillery report 3](./artillery_results_png/artillery_3.png)
 ![Artillery report 4](./artillery_results_png/artillery_4.png)
 ![Artillery report 5](./artillery_results_png/artillery_5.png)
 ![Artillery report 6](./artillery_results_png/artillery_6.png)

### Apache
 ![GetSpec](./apache_results_png/getSpecs.png)
 ![CalculatePrice_fixed](./apache_results_png/calculatePrice_fixed.png)
 ![CalculatePrice_minutes](./apache_results_png/calculatePrice_minutes.png)
 